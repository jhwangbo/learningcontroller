//
// Created by joonho on 03.08.18.
//

#ifndef LEARNINGCONTROLLER_HIERARCHICALCONTROLLER_HPP
#define LEARNINGCONTROLLER_HIERARCHICALCONTROLLER_HPP
#include <Eigen/Core>
#include "string"
#include "SimpleMLPLayer.hpp"

namespace RAI {

class QuadrupedController {

  static constexpr unsigned int stateDimension = 51; //master state dim
  static constexpr unsigned int actionDimension = 12;
  static constexpr int HistoryLength = 12;
  static constexpr int options = 3;

  typedef typename Eigen::Matrix<double, 3, 1> AngularVelocity;
  typedef typename Eigen::Matrix<double, 3, 1> LinearVelocity;
  typedef typename Eigen::Matrix<double, 4, 1> Quaternion;
  typedef typename Eigen::Matrix<double, 3, 3> RotationMatrix;
  typedef typename Eigen::Matrix<double, 4, 1> Vector4d;
  typedef typename Eigen::Matrix<double, stateDimension, 1> State;
  typedef typename Eigen::Matrix<double, actionDimension, 1> Action;
  using Dtype = double;

  using JointSpaceVector = Eigen::Matrix<double, 12, 1>;

  typedef typename Eigen::VectorXd VectorXd;

 public:
  QuadrupedController()
      : mlpNominal_("tanh", {256, 128}),
        mlpStabilizer_("tanh", {128, 128}),
        mlpStabilizer2_("tanh", {128, 128}) { // loco, stand, recovery

    jointVelHist_.setZero();
    jointPosHist_.setZero();

    jointNominalConfig << 0.0, 0.4, -0.8, 0.0, 0.4, -0.8, 0.0, -0.4, 0.8, 0.0, -0.4, 0.8;

    desJointPos_ = jointNominalConfig;
    scaledAction_.setZero(); // action: position difference
    previousAction_.setZero(); // action: position difference
    bodyVel_.setZero();
    bodyAngVel_.setZero();

    stateOffset_ << VectorXd::Constant(2, 0.0), 0.0, /// command
        0.0, 0.0, 0.0, /// gravity axis
        VectorXd::Constant(6, 0.0), /// body lin/ang vel
        jointNominalConfig.template cast<Dtype>(), /// joint position
        VectorXd::Constant(12, 0.0), /// position error history
        VectorXd::Constant(12, 0.0), /// joint velocity history
        VectorXd::Constant(options, 0.5); /// prev. action

    stateScale_ << 1.0, 1.0 / 0.3, 1.0, /// command
        VectorXd::Constant(3, 1.0), /// gravity axis
        1.0 / 1.5, 1.0 / 0.5, 1.0 / 0.5, 1.0 / 2.5, 1.0 / 2.5, 1.0 / 2.5, /// linear and angular velocities
        VectorXd::Constant(12, 1 / 1.0), /// joint angles
        VectorXd::Constant(12, 1 / 10.0), /// joint velocities
        VectorXD::Constant(12, 1.0 / 1.0),
        VectorXd::Constant(options, 1.0); /// prev. action


    stateScale_ << 1.0, 1.0 / 0.3, 1.0, /// command
        VectorXD::Constant(3, 1.0), /// gravity axis
        1.0 / 1.5, 1.0 / 0.5, 1.0 / 0.5, 1.0 / 2.5, 1.0 / 2.5, 1.0 / 2.5, /// linear and angular velocities
        VectorXD::Constant(12, 1 / 1.0), /// joint angles
        VectorXD::Constant(12, 1 / 10.0), /// joint velocities
        VectorXD::Constant(12, 1.0 / 1.0),
        VectorXD::Constant(Options, 1.0); /// prev. action


    actionScale_.setConstant(0.5);
    actionOffset_.setZero();
    actionMax.setConstant(3.14);
    actionMin.setConstant(-3.14);

    controlCounter = 0;
    masterCounter = 0;

    option_ = 0;
    intervals.push_back(locomotionInterval);
    intervals.push_back(standingInterval);
    intervals.push_back(standingInterval);
  }

  ~QuadrupedController() {}

  JointSpaceVector getDesJointPos(const Eigen::Matrix<double, 19, 1> &generalizedCoordinates,
                                  const Eigen::Matrix<double, 18, 1> &generalizedVelocities,
                                  double headingVel, double lateralVel, double yawrate) {

    /// update Option
    if (masterCounter % masterInterval == 0) {
      conversion_master(state_, generalizedCoordinates, generalizedVelocities);

      Eigen::Matrix<double, options, 1> masterAction;
      masterAction = master_.forward(state_);
      int argmax = 0;
      for (int i = 0; i < options; i++)
        if (masterAction[argmax] < masterAction[i]) argmax = i;

      option_ = argmax;
      masterCounter = 0;
    }

    Eigen::Matrix<double, 12, 1> controllerOutput;
    Eigen::Matrix<double, -1, 1> state_in;

    /// update target position in control frequency
    if (controlCounter % intervals[option_] == 0) {

      if(option_==0){ // locomotion
        conversion_0(state_in, generalizedCoordinates,generalizedVelocities, headingVel, lateralVel, yawrate);
        controllerOutput = mlpNominal_.forward(state_in);
        controllerOutput = controllerOutput.cwiseProduct(actionScale_);
        controllerOutput += jointNominalConfig;
      }else if (option_ == 1 || option_ == 2){
        conversion_1(state_in, generalizedCoordinates,generalizedVelocities);

        if(option_ == 1)controllerOutput = mlpStabilizer_.forward(state_in);
       else if (option_ == 2) controllerOutput = mlpStabilizer2_.forward(state_in);

        controllerOutput = controllerOutput.cwiseProduct(actionScale_);
        controllerOutput = controllerOutput.cwiseMin(actionMax);
        controllerOutput = controllerOutput.cwiseMax(actionMin);
        controllerOutput += generalizedCoordinates.tail(12);
      }

      controlCounter = 0;
    }


    desJointPos_ = controllerOutput.template cast<double>();
    previousAction_ = controllerOutput - generalizedCoordinates.tail(12);


    /// update History
    tempHist_ = jointVelHist_;
    jointVelHist_.head(HistoryLength * 12 - 12) = tempHist_.tail(HistoryLength * 12 - 12);
    jointVelHist_.tail(12) = generalizedVelocities.tail(12).template cast<Dtype>();
    tempHist_ = jointPosHist_;
    jointPosHist_.head(HistoryLength * 12 - 12) = tempHist_.tail(HistoryLength * 12 - 12);
    jointPosHist_.tail(12) = desJointPos_ - generalizedCoordinates.tail(12).template cast<Dtype>();

    /// increment counter
    controlCounter++;
    masterCounter++;

    return desJointPos_;
  };

  RAI::MLP_fullyconnected<stateDimension, actionDimension> *getMlpNominalPtr() {
    return &mlpNominal_;
  }

  RAI::MLP_fullyconnected<stateDimension, actionDimension> *getMlpStabilizerPtr() {
    return &mlpStabilizer_;
  }

 private:

  inline void conversion_master(State &state,
                                const VectorXd &q,
                                const VectorXd &u,
                                double headingVel,
                                double lateralVel,
                                double yawrate) {

    Quaternion quat = q.template segment<4>(3);
    RotationMatrix R_b = quatToRotMat(quat);
    /// command slots
    state_unscaled_[0] = headingVel;
    state_unscaled_[1] = lateralVel;
    state_unscaled_[2] = yawrate;

    /// gravity vector
    state_unscaled_.template segment<3>(3) = R_b.row(2).transpose().template cast<Dtype>();

    /// velocity in body coordinate
    LinearVelocity bodyVel = R_b.transpose() * u.template segment<3>(0);
    AngularVelocity bodyAngVel = u.template segment<3>(3);
    VectorXd jointVel = u.template segment<12>(6);

    /// velocities
    state_unscaled_.template segment<3>(6) = bodyVel.template cast<Dtype>();
    state_unscaled_.template segment<3>(9) = bodyAngVel.template cast<Dtype>();

    /// joint positions
    state_unscaled_.template segment<12>(12) = q.template segment<12>(7).
        template cast<Dtype>();

    /// joint velocities
    state_unscaled_.template segment<12>(24) = jointVel.template cast<Dtype>();

    /// previous action
    state_unscaled_.template segment<12>(36) = previousAction_;

    for(int i = 0; i<Options; i++) {
      if(option_==i) state_unscaled[48 + i] = 1.0;
      else state_unscaled[48 + i] = 0.0;// argmax
    }

    state = (state_unscaled_ - stateOffset_).cwiseProduct(stateScale_);
  }

  inline void conversion_0(Eigen::Matrix<-1, 1> &state,
                                                        const VectorXd &q,
                                                        const VectorXd &u,
                                                        double headingVel, double lateralVel, double yawrate) {
    Quaternion quat = q.template segment<4>(3);
    RotationMatrix R_b = quatToRotMat(quat);

    /// velocity in body coordinate
    LinearVelocity bodyVel = R_b.transpose() * u.template segment<3>(0);
    AngularVelocity bodyAngVel = u.template segment<3>(3);
    VectorXd jointVel = u.template segment<12>(6);
    Eigen::Matrix<double, -1, 1> state_unscaled;
    Eigen::Matrix<double, -1, 1> state_offset;
    Eigen::Matrix<double, -1, 1> state_scale;

    state_unscaled.resize(97);
    state_offset.resize(97);
    state_scale.resize(97);
    state.resize(97);

    state_offset << 0.0, 0.0, 0.0,
        0.44,
        VectorXd::Constant(6, 0.0),
        jointNominalConfig,
        VectorXd::Constant(24, 0.0),
        VectorXd::Constant(36, 0.0),
        jointNominalConfig,
        0.0, 0.0, 0.0;

    state_scale << VectorXD::Constant(3, 1.0 / 0.7), 1.0 / 0.12,
        1.0 / 1.5, 1.0 / 0.5, 1.0 / 0.5, 1.0 / 2.5, 1.0 / 2.5, 1.0 / 2.5,
        VectorXD::Constant(36, 1.0 / 1.0),
        VectorXD::Constant(36, 1.0 / 10.0),
        VectorXD::Constant(12, 1.0 / 1.0),
        1.0, 1.0 / 0.3, 1.0;

    state_unscaled_.head(3) = R_b.row(2).transpose().template cast<Dtype>();
    state_unscaled_(3) = q(2);
    state_unscaled.template segment<3>(4) = bodyVel.template cast<Dtype>();
    state_unscaled.template segment<3>(7) = bodyAngVel.template cast<Dtype>();
    state_unscaled.template segment<12>(10) = jointPosHist_.template segment<12>(48);
    state_unscaled.template segment<12>(22) = jointPosHist_.template segment<12>(96);
    state_unscaled.template segment<12>(34) = q.template segment<12>(7).template cast<Dtype>();

    state_unscaled.template segment<12>(46) = jointVelHist_.template segment<12>(48);
    state_unscaled.template segment<12>(58) = jointVelHist_.template segment<12>(96);
    state_unscaled.template segment<12>(70) = u.template segment<12>(6).template cast<Dtype>();
    state_unscaled.template segment<12>(82) = desJointPos_; //TODO:!!!!!

    state_unscaled[94] = headingVel;
    state_unscaled[95] = lateralVel;
    state_unscaled[96] = yawrate;

    state = (state_unscaled - stateOffset).cwiseProduct(stateScale);
  }

  inline void conversion_1(Eigen::Matrix<-1, 1> &state,
                           const VectorXd &q,
                           const VectorXd &u) {

    Quaternion quat = q.template segment<4>(3);
    RotationMatrix R_b = quatToRotMat(quat);

    /// velocity in body coordinate
    LinearVelocity bodyVel = R_b.transpose() * u.template segment<3>(0);
    AngularVelocity bodyAngVel = u.template segment<3>(3);
    VectorXd jointVel = u.template segment<12>(6);
    Eigen::Matrix<double, -1, 1> state_unscaled;
    Eigen::Matrix<double, -1, 1> state_offset;
    Eigen::Matrix<double, -1, 1> state_scale;

    state_unscaled.resize(96);
    state_offset.resize(96);
    state_scale.resize(96);
    state.resize(96);

    state_offset << VectorXd::Constant(2, 0.0), 0.0, /// command
        0.0, 0.0, 0.0, /// gravity axis
        VectorXd::Constant(6, 0.0), /// body lin/ang vel
        jointNominalConfig.template cast<Dtype>(), /// joint position
        VectorXd::Constant(24, 0.0), /// position error history
        VectorXd::Constant(36, 0.0), /// joint velocity history
        VectorXd::Constant(12, 0.0); /// prev. action

    state_scale << 1.0, 1.0 / 0.3, 1.0, /// command
        VectorXd::Constant(3, 1.0), /// gravity axis
        1.0 / 1.5, 1.0 / 0.5, 1.0 / 0.5, 1.0 / 2.5, 1.0 / 2.5, 1.0 / 2.5, /// linear and angular velocities
        VectorXd::Constant(36, 1 / 1.0), /// joint angles
        VectorXd::Constant(36, 1 / 10.0), /// joint velocities
        VectorXd::Constant(12, 1.0 / 1.0); /// prev. action

    /// command slots
    state_unscaled[0] = 0.0;
    state_unscaled[1] = 0.0;
    state_unscaled[2] = 0.0;

    /// gravity vector
    state_unscaled.template segment<3>(3) = R_b.row(2).transpose().template cast<Dtype>();

    /// velocities
    state_unscaled.template segment<3>(6) = bodyVel.template cast<Dtype>();
    state_unscaled.template segment<3>(9) = bodyAngVel.template cast<Dtype>();

    /// joint positions
    state_unscaled.template segment<12>(12) = q.template segment<12>(7).template cast<Dtype>();

    /// joint positions history
    state_unscaled.template segment<12>(24) = jointPosHist_.template segment<12>(12 * HistoryLength - 36);
    state_unscaled.template segment<12>(36) = jointPosHist_.template segment<12>(12 * HistoryLength - 60);

    /// joint velocities
    state_unscaled.template segment<12>(48) = jointVel.template cast<Dtype>();

    /// joint velocities history
    state_unscaled.template segment<12>(60) = jointVelHist_.template segment<12>(12 * HistoryLength - 36);
    state_unscaled.template segment<12>(72) = jointVelHist_.template segment<12>(12 * HistoryLength - 60);

    /// previous action
    state_unscaled.template segment<12>(84) = previousAction_;

    state = (state_unscaled - stateOffset).cwiseProduct(stateScale);
  }

  inline RotationMatrix quatToRotMat(Quaternion &q) {
    RotationMatrix R;
    R << q(0) * q(0) + q(1) * q(1) - q(2) * q(2) - q(3) * q(3), 2 * q(1) * q(2) - 2 * q(0) * q(3), 2 * q(0) * q(2)
        + 2 * q(1) * q(3),
        2 * q(0) * q(3) + 2 * q(1) * q(2), q(0) * q(0) - q(1) * q(1) + q(2) * q(2) - q(3) * q(3), 2 * q(2) * q(3)
        - 2 * q(0) * q(1),
        2 * q(1) * q(3) - 2 * q(0) * q(2), 2 * q(0) * q(1) + 2 * q(2) * q(3), q(0) * q(0) - q(1) * q(1) - q(2) * q(2)
        + q(3) * q(3);
    return R;
  }

  State state_;
  Action action_, previousAction_;
  JointSpaceVector torque_, jointNominalConfig;
  LinearVelocity bodyVel_;
  AngularVelocity bodyAngVel_;
  State state_unscaled_; /// just memory slot for computation
  State stateOffset_;
  State stateScale_;
  Action actionOffset_;
  Action actionScale_;
  Action desJointPos_;
  Action scaledAction_;
  Action actionMax, actionMin;
  Eigen::Matrix<Dtype, 12 * HistoryLength, 1> jointVelHist_, jointPosHist_, tempHist_;
  unsigned long int controlCounter = 0;
  unsigned long int masterCounter = 0;
  unsigned long int locomotionInterval = 2;
  unsigned long int standingInterval = 4;
  unsigned long int masterInterval = 10;

  std::vector<int> intervals;
  int option_;

  RAI::MLP_fullyconnected<97, actionDimension> mlpNominal_; // walk
  RAI::MLP_fullyconnected<96, actionDimension> mlpStabilizer_; //stand
  RAI::MLP_fullyconnected<96, actionDimension> mlpStabilizer2_; // recovery
  RAI::MLP_fullyconnected<stateDimension, options> master_;

};

}

#endif //LEARNINGCONTROLLER_HIERARCHICALCONTROLLER_HPP
