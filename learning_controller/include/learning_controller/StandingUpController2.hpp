//
// Created by jhwangbo on 11.05.17.
//

#ifndef RAI_QUADRUPEDCONTROLLER_HPP
#define RAI_QUADRUPEDCONTROLLER_HPP

#include <Eigen/Core>
#include "string"
#include "SimpleMLPLayer.hpp"

namespace RAI {

class QuadrupedController {

  static constexpr unsigned int stateDimension = 96;
  static constexpr unsigned int actionDimension = 12;
  static constexpr int HistoryLength = 12;

  typedef typename Eigen::Matrix<double, 3, 1> AngularVelocity;
  typedef typename Eigen::Matrix<double, 3, 1> LinearVelocity;
  typedef typename Eigen::Matrix<double, 4, 1> Quaternion;
  typedef typename Eigen::Matrix<double, 3, 3> RotationMatrix;
  typedef typename Eigen::Matrix<double, 4, 1> Vector4d;
  typedef typename Eigen::Matrix<double, stateDimension, 1> State;
  typedef typename Eigen::Matrix<double, actionDimension, 1> Action;
  using Dtype = double;

  using JointSpaceVector = Eigen::Matrix<double, 12, 1>;

  typedef typename Eigen::VectorXd VectorXd;

 public:
  QuadrupedController()
      : mlpNominal_("tanh", {128, 128}), mlpStabilizer_("tanh", {128, 128}) {

    jointVelHist_.setZero();
    jointPosHist_.setZero();

    jointNominalConfig << 0.0, 0.4, -0.8, 0.0, 0.4, -0.8, 0.0, -0.4, 0.8, 0.0, -0.4, 0.8;

    desJointPos_ = jointNominalConfig;
    scaledAction_.setZero(); // action: position difference
    previousAction_.setZero(); // action: position difference
    bodyVel_.setZero();
    bodyAngVel_.setZero();

    stateOffset_ << VectorXd::Constant(2, 0.0), 0.0, /// command
        0.0, 0.0, 0.0, /// gravity axis
        VectorXd::Constant(6, 0.0), /// body lin/ang vel
        jointNominalConfig.template cast<Dtype>(), /// joint position
        VectorXd::Constant(24, 0.0), /// position error history
        VectorXd::Constant(36, 0.0), /// joint velocity history
        VectorXd::Constant(12, 0.0); /// prev. action

    stateScale_ << 1.0, 1.0 / 0.3, 1.0, /// command
        VectorXd::Constant(3, 1.0), /// gravity axis
        1.0 / 1.5, 1.0 / 0.5, 1.0 / 0.5, 1.0 / 2.5, 1.0 / 2.5, 1.0 / 2.5, /// linear and angular velocities
        VectorXd::Constant(36, 1 / 1.0), /// joint angles
        VectorXd::Constant(36, 1 / 10.0), /// joint velocities
        VectorXd::Constant(12, 1.0 / 1.0); /// prev. action

    actionScale_.setConstant(0.5);
    actionOffset_.setZero();
    actionMax.setConstant(3.14);
    actionMin.setConstant(-3.14);

    controlCounter = 0;


    //// standing up heuristic
    jointStable << 0.0, M_PI_2, -2.45, 0.0, M_PI_2, -2.45, 0.0, -M_PI_2, 2.45, 0.0, -M_PI_2, 2.45;


    stableCounter = 0;
  }

  ~QuadrupedController() {}

  JointSpaceVector getDesJointPos(const Eigen::Matrix<double, 19, 1> &generalizedCoordinates,
                                  const Eigen::Matrix<double, 18, 1> &generalizedVelocities) {

    /// update target position in control frequency
    if (controlCounter % controlInterval == 0) {
      conversion_GeneralizedState2LearningState(state_, generalizedCoordinates, generalizedVelocities);


      Quaternion quat = generalizedCoordinates.template segment<4>(3);
      RotationMatrix R_b = quatToRotMat(quat);

//      Dtype jointError = 0;
//      for (size_t i = 0; i < 12; i++) {
//        jointError += std::abs(generalizedCoordinates[7 + i] - jointStable[i]);
//      }

      if (R_b(2, 2) > 0.96) {
        stableCounter++;
      }
      if (R_b(2, 2) < 0.8 || (generalizedCoordinates[2] < 0.3 && stableCounter > 300) )stableCounter = 0; // back to recovery controller

      if (stableCounter > 200) {
        action_ = mlpStabilizer_.forward(state_);
      } else {
        action_ = mlpNominal_.forward(state_);
      }

      scaledAction_ = action_.cwiseProduct(actionScale_) + actionOffset_;

      ///clipping action (within [-pi,pi])
      desJointPos_ = scaledAction_.cwiseMin(actionMax);
      desJointPos_ = desJointPos_.cwiseMax(actionMin);
      desJointPos_ += generalizedCoordinates.tail(12);

      previousAction_ = scaledAction_; /// !!!note: not target position. raw action
      controlCounter = 0;
    }

    /// update History
    tempHist_ = jointVelHist_;
    jointVelHist_.head(HistoryLength * 12 - 12) = tempHist_.tail(HistoryLength * 12 - 12);
    jointVelHist_.tail(12) = generalizedVelocities.tail(12).template cast<Dtype>();
    tempHist_ = jointPosHist_;
    jointPosHist_.head(HistoryLength * 12 - 12) = tempHist_.tail(HistoryLength * 12 - 12);
    jointPosHist_.tail(12) = desJointPos_ - generalizedCoordinates.tail(12).template cast<Dtype>();

    /// increment counter
    controlCounter++;

    return desJointPos_;
  };

  RAI::MLP_fullyconnected<stateDimension, actionDimension> *getMlpNominalPtr() {
    return &mlpNominal_;
  }


  RAI::MLP_fullyconnected<stateDimension, actionDimension>* getMlpStabilizerPtr() {
    return &mlpStabilizer_;
  }

 private:

  inline void conversion_GeneralizedState2LearningState(State &state,
                                                        const VectorXd &q,
                                                        const VectorXd &u) {
    Quaternion quat = q.template segment<4>(3);
    RotationMatrix R_b = quatToRotMat(quat);
    /// command slots
    state_unscaled_[0] = 0.0;
    state_unscaled_[1] = 0.0;
    state_unscaled_[2] = 0.0;

    /// gravity vector
    state_unscaled_.template segment<3>(3) = R_b.row(2).transpose().template cast<Dtype>();

    /// velocity in body coordinate
    LinearVelocity bodyVel = R_b.transpose() * u.template segment<3>(0);
    AngularVelocity bodyAngVel = u.template segment<3>(3);
    VectorXd jointVel = u.template segment<12>(6);

    /// velocities
    state_unscaled_.template segment<3>(6) = bodyVel.template cast<Dtype>();
    state_unscaled_.template segment<3>(9) = bodyAngVel.template cast<Dtype>();

    /// joint positions
    state_unscaled_.template segment<12>(12) = q.template segment<12>(7).template cast<Dtype>();

    /// joint positions history
    state_unscaled_.template segment<12>(24) = jointPosHist_.template segment<12>(12 * HistoryLength - 36);
    state_unscaled_.template segment<12>(36) = jointPosHist_.template segment<12>(12 * HistoryLength - 60);

    /// joint velocities
    state_unscaled_.template segment<12>(48) = jointVel.template cast<Dtype>();

    /// joint velocities history
    state_unscaled_.template segment<12>(60) = jointVelHist_.template segment<12>(12 * HistoryLength - 36);
    state_unscaled_.template segment<12>(72) = jointVelHist_.template segment<12>(12 * HistoryLength - 60);

    /// previous action
    state_unscaled_.template segment<12>(84) = previousAction_;

    state = (state_unscaled_ - stateOffset_).cwiseProduct(stateScale_);
  }

  inline RotationMatrix quatToRotMat(Quaternion &q) {
    RotationMatrix R;
    R << q(0) * q(0) + q(1) * q(1) - q(2) * q(2) - q(3) * q(3), 2 * q(1) * q(2) - 2 * q(0) * q(3), 2 * q(0) * q(2)
        + 2 * q(1) * q(3),
        2 * q(0) * q(3) + 2 * q(1) * q(2), q(0) * q(0) - q(1) * q(1) + q(2) * q(2) - q(3) * q(3), 2 * q(2) * q(3)
        - 2 * q(0) * q(1),
        2 * q(1) * q(3) - 2 * q(0) * q(2), 2 * q(0) * q(1) + 2 * q(2) * q(3), q(0) * q(0) - q(1) * q(1) - q(2) * q(2)
        + q(3) * q(3);
    return R;
  }

  State state_;
  Action action_, previousAction_;
  JointSpaceVector torque_, jointNominalConfig;
  LinearVelocity bodyVel_;
  AngularVelocity bodyAngVel_;
  State state_unscaled_; /// just memory slot for computation
  State stateOffset_;
  State stateScale_;
  Action actionOffset_;
  Action actionScale_;
  Action desJointPos_;
  Action scaledAction_;
  Action actionMax, actionMin;
  Eigen::Matrix<Dtype, 12 * HistoryLength, 1> jointVelHist_, jointPosHist_, tempHist_;
  unsigned long int controlCounter = 0;
  unsigned long int controlInterval = 20;
  unsigned long int stableCounter = 0;
  Eigen::Matrix<Dtype, 12, 1> jointStable;

  RAI::MLP_fullyconnected<stateDimension, actionDimension> mlpNominal_;
  RAI::MLP_fullyconnected<stateDimension,actionDimension> mlpStabilizer_;

};

}

#endif //RAI_QUADRUPEDCONTROLLER_HPP
