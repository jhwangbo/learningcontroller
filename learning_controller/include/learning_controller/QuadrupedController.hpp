//
// Created by jhwangbo on 11.05.17.
//

#ifndef RAI_QUADRUPEDCONTROLLER_HPP
#define RAI_QUADRUPEDCONTROLLER_HPP

#include <Eigen/Core>
#include "string"
#include "SimpleMLPLayer.hpp"

namespace RAI {

class QuadrupedController{

  static constexpr unsigned int stateDimension = 97;
  static constexpr unsigned int actionDimension = 12;
  static constexpr int HistoryLength = 12;

  typedef typename Eigen::Matrix<double, 3, 1> AngularVelocity;
  typedef typename Eigen::Matrix<double, 3, 1> LinearVelocity;
  typedef typename Eigen::Matrix<double, 4, 1> Quaternion;
  typedef typename Eigen::Matrix<double, 3, 3> RotationMatrix;
  typedef typename Eigen::Matrix<double, 4, 1> Vector4d;
  typedef typename Eigen::Matrix<double, stateDimension, 1> State;
  typedef typename Eigen::Matrix<double, actionDimension, 1> Action;
  using Dtype = double;

  using JointSpaceVector = Eigen::Matrix<double, 12, 1>;

  typedef typename Eigen::VectorXd VectorXd;

 public:
  QuadrupedController()
      : mlpNominal_("tanh", { 256, 128 }),
        mlpStabilizer_("tanh", { 256, 128 }) {

	  command_.resize(236, 3);

	  command_ << -0.603556, 0.388189, 0.760587,
			  -0.609857, 0.047582, -0.745268,
			  -0.346321, 0.346874, -0.903136,
			  0.760676, 0.176275, 0.770392,
			  -0.057796, -0.012769, 0.330954,
			  -0.192061, 0.111225, -1.161312,
			  -0.641537, 0.310109, 0.950291,
			  0.937850, -0.241011, 0.036900,
			  -0.185089, -0.083707, 0.106853,
			  0.688975, 0.323740, 0.255461,
			  0.230650, -0.078119, 0.625047,
			  -0.246778, 0.127085, 0.852832,
			  0.754363, 0.321078, -0.281116,
			  0.569705, 0.316305, -0.996842,
			  -0.070091, 0.122531, 0.561296,
			  0.627954, -0.313251, -0.403226,
			  -0.141523, 0.094473, -0.307864,
			  -0.331341, 0.053715, 0.787717,
			  0.193294, 0.369572, -0.776355,
			  0.803982, 0.196884, -0.889152,
			  0.404133, 0.130013, 0.911721,
			  -0.245090, 0.018651, -1.094210,
			  0.469912, -0.192085, 0.448128,
			  0.908206, 0.369595, 0.561055,
			  0.085626, 0.032163, -0.150786,
			  0.080212, -0.375784, -0.288387,
			  -0.377781, 0.157052, 1.151178,
			  -0.857531, 0.015773, -0.242416,
			  -0.814022, 0.312029, -0.823661,
			  -0.073022, -0.135838, -0.417518,
			  -0.981335, -0.216239, -0.446251,
			  0.830052, -0.308841, 0.946802,
			  0.285483, -0.151262, -0.607142,
			  -0.997162, -0.217254, -0.454370,
			  -0.939229, 0.121598, -0.218715,
			  -0.090068, -0.179655, -0.855270,
			  -0.982705, 0.304053, -1.000426,
			  0.454159, -0.044536, -0.091829,
			  -0.291767, 0.204731, -1.127066,
			  0.560892, 0.082637, 0.607681,
			  -0.126687, 0.226613, 0.480102,
			  -0.126890, -0.308855, -0.685171,
			  -0.901574, 0.382851, 0.431771,
			  -0.817800, -0.359483, 0.841629,
			  0.188074, -0.027039, 0.140557,
			  -0.517832, -0.139477, 0.964257,
			  0.682738, 0.104164, -0.193158,
			  0.714426, -0.215761, -0.340492,
			  -0.022200, 0.082525, -0.585691,
			  -0.559380, 0.079903, 1.030006,
			  -0.547583, -0.041258, -0.079784,
			  0.073576, -0.371661, -0.590381,
			  -0.304866, -0.073816, 0.486072,
			  -0.077536, -0.313563, -0.234408,
			  0.278648, -0.032099, -0.763584,
			  -0.676855, 0.040912, 0.202083,
			  0.431271, 0.244323, -0.303411,
			  0.155478, 0.160680, -0.667933,
			  0.768486, -0.358246, 0.053358,
			  -0.213896, -0.224255, -0.159785,
			  -0.642050, -0.032286, 0.579130,
			  0.266667, 0.366827, -1.030921,
			  0.248001, 0.232036, 0.833600,
			  -0.344117, -0.038500, 0.431713,
			  0.605931, -0.133257, -0.872036,
			  0.961956, 0.192724, -0.720398,
			  -0.745926, 0.005436, 0.257616,
			  -0.535520, -0.240060, 0.103308,
			  0.214865, -0.265048, -1.186433,
			  -0.185081, -0.105319, 0.635490,
			  0.768154, 0.353454, -0.189434,
			  0.096266, -0.386262, -1.063648,
			  -0.261994, 0.263245, 0.205793,
			  -0.583308, 0.101273, -0.782027,
			  -0.118113, 0.030997, 0.548666,
			  0.912392, 0.120406, 0.082299,
			  -0.751948, 0.181304, -0.592646,
			  -0.058474, -0.324409, 1.000936,
			  -0.913219, -0.388510, 0.928874,
			  0.383250, -0.164558, -1.034884,
			  -0.433464, 0.341035, 0.568974,
			  -0.732439, -0.345456, 0.472116,
			  0.370559, 0.064875, 0.664783,
			  0.818909, 0.109721, 0.004568,
			  0.221738, 0.121015, -0.178808,
			  -0.613132, -0.355238, 0.853853,
			  0.508849, 0.253484, 0.409913,
			  -0.307479, 0.023138, 0.056621,
			  -0.162749, 0.155480, -0.482843,
			  -0.688560, -0.230076, 0.489526,
			  0.638001, 0.034624, -0.284134,
			  0.249847, 0.162016, 0.162444,
			  0.477121, 0.365148, 0.930866,
			  0.610225, -0.044366, 0.823078,
			  -0.865555, -0.331682, 0.957117,
			  -0.004846, 0.103560, 0.757044,
			  0.510292, 0.236943, -1.196741,
			  0.484810, 0.152953, -1.192582,
			  -0.686996, 0.357453, -0.574254,
			  -0.085383, 0.016152, -1.145283,
			  0.236201, 0.363050, -0.182196,
			  0.864367, -0.341123, -0.381444,
			  0.790847, 0.220022, 1.022806,
			  0.165037, 0.331350, -0.483601,
			  0.165494, 0.226041, -0.388596,
			  -0.930269, -0.278523, -0.382852,
			  0.770840, 0.278328, -0.868511,
			  -0.184538, 0.227884, 0.018717,
			  -0.927236, -0.183335, 0.855976,
			  0.492296, -0.217751, -0.277647,
			  -0.690342, -0.143181, 0.469658,
			  -0.712183, 0.263649, 0.306970,
			  0.211918, 0.257746, -0.119068,
			  -0.351692, 0.057464, 1.079295,
			  -0.196418, -0.171185, -0.999606,
			  0.219604, -0.046729, 0.210171,
			  -0.666219, -0.043028, 0.906322,
			  -0.623816, -0.027470, -0.074159,
			  -0.810742, -0.176769, -0.150196,
			  -0.353627, 0.140300, 0.590844,
			  0.480731, 0.197758, -0.080371,
			  0.385637, -0.191591, -0.004549,
			  0.655955, -0.294535, -0.649275,
			  -0.413264, -0.301199, -0.994675,
			  -0.381262, -0.247278, -1.038280,
			  0.046060, -0.283414, 0.932138,
			  -0.349402, 0.068035, -0.640398,
			  0.663685, -0.341311, 0.867830,
			  0.620590, 0.257861, 0.508164,
			  0.113997, 0.178322, 0.894751,
			  -0.474072, 0.340686, 1.051205,
			  0.361132, -0.005889, -0.864746,
			  -0.532694, 0.123906, -0.254640,
			  -0.087149, 0.312099, 1.153351,
			  -0.230866, 0.030820, 0.347506,
			  0.077203, -0.174236, 0.951383,
			  0.983407, 0.380766, -0.042647,
			  0.510441, -0.370860, -1.166177,
			  0.960910, -0.139004, 0.294913,
			  -0.897128, -0.152680, 0.539981,
			  0.513751, -0.303270, 0.257798,
			  0.203960, 0.332613, 0.212079,
			  0.714337, -0.291617, -0.159756,
			  0.858969, 0.317984, -0.170495,
			  -0.180970, -0.000281, -1.175574,
			  0.081757, 0.066506, 1.099140,
			  -0.561433, -0.376534, -1.114583,
			  -0.348387, 0.022306, 0.926964,
			  -0.808101, -0.374342, -0.607341,
			  0.495067, 0.261714, -1.178604,
			  0.497018, -0.128011, 0.755809,
			  0.086599, 0.277369, -0.862801,
			  -0.323735, -0.203144, 0.911679,
			  0.664667, 0.065193, -0.971096,
			  -0.286993, -0.383506, 0.402436,
			  0.092804, 0.145183, 0.355265,
			  -0.306636, 0.078903, -0.159914,
			  0.245606, -0.308776, -0.864579,
			  0.491750, 0.094280, -0.619712,
			  -0.748928, -0.343829, 0.361102,
			  0.644788, -0.344577, 0.857698,
			  -0.949699, -0.291194, -0.997511,
			  -0.171142, 0.231113, 1.133014,
			  0.462815, -0.326081, -1.124496,
			  0.562748, -0.209705, 0.804971,
			  -0.740806, 0.186994, -0.428465,
			  -0.549864, 0.120424, 0.735520,
			  -0.299972, 0.013016, 0.243357,
			  -0.425831, -0.138889, 0.695089,
			  0.185334, -0.281746, -0.520323,
			  0.676812, 0.371433, -0.024827,
			  -0.664878, 0.376298, 1.134845,
			  0.004401, -0.300912, 0.596376,
			  -0.289186, 0.125355, -0.482486,
			  -0.905845, -0.167852, -0.585337,
			  -0.572679, 0.203629, 0.927753,
			  -0.332664, -0.057766, 0.758369,
			  -0.540795, -0.186245, -0.963990,
			  0.872240, 0.202989, 0.863024,
			  -0.124054, -0.074536, 0.959845,
			  0.880673, 0.350653, 0.057854,
			  -0.988331, -0.195658, -0.911521,
			  0.220614, 0.026531, -0.773294,
			  0.602152, 0.363804, 0.494658,
			  -0.534037, -0.185802, 0.795263,
			  0.864937, -0.199932, -1.116399,
			  0.146927, -0.160480, -0.377111,
			  -0.341918, -0.237361, -0.376786,
			  0.169047, 0.001361, 0.535416,
			  -0.419075, 0.236764, 0.197835,
			  -0.194891, -0.213301, -0.189586,
			  0.724115, 0.080671, -0.979035,
			  0.229479, -0.310030, -1.142334,
			  -0.592602, 0.270273, -0.532159,
			  0.654418, 0.336632, -0.384583,
			  0.351723, -0.001418, -0.510361,
			  -0.502101, -0.177911, -0.789832,
			  -0.048429, 0.122016, -0.241768,
			  -0.201850, 0.333839, 0.474359,
			  0.198876, 0.007872, -0.711177,
			  0.601046, 0.379353, 0.399184,
			  -0.789862, -0.242177, -0.136641,
			  0.642884, -0.311052, -0.160091,
			  -0.290988, -0.082865, -0.736314,
			  -0.139861, -0.063395, 0.279411,
			  0.144478, -0.150820, -0.554375,
			  -0.221742, -0.163855, 0.430127,
			  -0.141395, -0.154803, 1.102513,
			  0.912689, -0.315551, 0.660802,
			  0.145943, 0.075062, 0.258545,
			  -0.447309, -0.275823, -1.056860,
			  0.244647, -0.399473, -0.555090,
			  0.176723, -0.173124, 1.168032,
			  0.926937, 0.040649, 0.653296,
			  -0.828195, 0.296722, -0.059150,
			  0.000998, -0.366197, 0.434159,
			  0.043179, 0.323778, -0.199357,
			  -0.819668, -0.295221, -0.287642,
			  0.809333, 0.266983, -0.688151,
			  0.768778, 0.240375, -0.280949,
			  -0.703070, 0.003786, -0.399906,
			  0.239632, -0.076033, 1.142028,
			  -0.478753, -0.261142, 0.133064,
			  -0.108688, 0.060147, 0.831129,
			  0.687999, 0.084974, -0.220648,
			  -0.607590, -0.228443, -0.091156,
			  -0.392297, 0.015946, 0.783136,
			  0.974975, -0.070863, 0.573652,
			  -0.681905, -0.372179, 0.161832,
			  -0.526240, -0.165735, 1.125066,
			  0.404473, 0.241153, 0.778799,
			  -0.249057, -0.122798, 1.103062,
			  0.947410, -0.333347, 0.351231,
			  0.720198, 0.191584, 0.988536,
			  -0.196233, 0.019792, -1.164351,
			  0.263862, 0.243617, -0.823935;


    jointVelHist_.setZero();
    jointPosHist_.setZero();

    jointNominalConfig << 0.0, 0.4, -0.8, 0.0, 0.4, -0.8, 0.0, -0.4, 0.8, 0.0, -0.4, 0.8;
    dampedAction_ = jointNominalConfig;
    scaledAction_ = jointNominalConfig;
    previousAction_ = jointNominalConfig;
    bodyVel_.setZero();
    bodyAngVel_.setZero();
    headingVelDamped_=0; lateralVelDamped_ = 0; yawrateDamped_ = 0;

    dampedState_.setZero();

    stateOffset_ << 0.0, 0.0, 0.0, /// gravity axis 3
        0.44, /// average height
        VectorXd::Constant(6, 0.0), /// body lin/ang vel 6
        jointNominalConfig.template cast<Dtype>(), /// joint position 12
        VectorXd::Constant(24, 0.0), /// position error
        VectorXd::Constant(36, 0.0), /// joint position history
        jointNominalConfig.template cast<Dtype>(),
        0.0,0.0,0.0;  /// previous command

    stateScale_ << VectorXd::Constant(3, 1.0 / 0.7), /// gravity axes angles
        1.0 / 0.12, /// average height
        1.0 / 1.5, 1.0 / 0.5, 1.0 / 0.5, 1.0 / 2.5, 1.0 / 2.5, 1.0 / 2.5, /// linear and angular velocities
        VectorXd::Constant(36, 1.0 / 1.0), /// joint angles
        VectorXd::Constant(36, 1.0 / 10.0), /// joint velocities
        VectorXd::Constant(12, 1.0 / 1.0),
        1.0, 1.0/0.3, 1.0; /// previous command


    actionScale_.setConstant(0.5);
    actionOffset_ = jointNominalConfig.template cast<Dtype>();
    dampedJointPos_ = jointNominalConfig.template cast<Dtype>();
  }

  ~QuadrupedController(){}

  JointSpaceVector getDesJointPos( const Eigen::Matrix<double, 19, 1> &generalizedCoordinates,
                                   const Eigen::Matrix<double, 18, 1> &generalizedVelocities,
                                   double headingVel, double lateralVel, double yawrate) {

	  controlCounter++;

	  if(controlCounter%800 == 0) {
		  controlRow++;
		  std::cout<<"command_(controlRow, 0) "<<command_.row(controlRow)<<std::endl;
	  }

	double hv = command_(controlRow, 0);
	double lv = command_(controlRow, 1);
	double yv = command_(controlRow, 2);


	  headingVelDamped_ = headingVelDamped_*0.95 + hv*0.05;
	lateralVelDamped_ = lateralVelDamped_*0.95 + lv*0.05;
	yawrateDamped_ = yawrateDamped_*0.95 + yv*0.05;

	double norm = std::sqrt(headingVelDamped_ * headingVelDamped_ + lateralVelDamped_*lateralVelDamped_ + yawrateDamped_*yawrateDamped_);


	if(norm > 0.3)
      conversion_GeneralizedState2LearningState(state_, generalizedCoordinates, generalizedVelocities, headingVelDamped_, lateralVelDamped_, yawrateDamped_);
	else
	  conversion_GeneralizedState2LearningState(state_, generalizedCoordinates, generalizedVelocities, 0, 0, 0);

    action_ = mlpNominal_.forward(state_);// + mlpStabilizer_.forward(state_);
    scaledAction_ = action_.cwiseProduct(actionScale_) + actionOffset_;
    dampedAction_ = scaledAction_;

    previousAction_ = dampedAction_;

    tempHist_ = jointVelHist_;
    jointVelHist_.head(HistoryLength*12-12) = tempHist_.tail(HistoryLength*12-12);
    jointVelHist_.tail(12) = generalizedVelocities.tail(12).template cast<Dtype>();
    tempHist_ = jointPosHist_;
    jointPosHist_.head(HistoryLength*12-12) = tempHist_.tail(HistoryLength*12-12) ;
    jointPosHist_.tail(12) = dampedAction_ - generalizedCoordinates.tail(12).template cast<Dtype>();

    return dampedAction_;
  };

  RAI::MLP_fullyconnected<stateDimension, actionDimension>* getMlpNominalPtr() {
    return &mlpNominal_;
  }

  RAI::MLP_fullyconnected<stateDimension, actionDimension>* getMlpStabilizerPtr() {
    return &mlpStabilizer_;
  }

 private:

  inline void conversion_GeneralizedState2LearningState(State &state,
                                                        const VectorXd &q,
                                                        const VectorXd &u,
                                                        double headingVel, double lateralVel, double yawrate) {
    Quaternion quat = q.template segment<4>(3);
    RotationMatrix R_b = quatToRotMat(quat);
    state_unscaled_.head(3) = R_b.row(2).transpose().template cast<Dtype>();
    state_unscaled_(3) = q(2);
    /// velocity in body coordinate
    LinearVelocity bodyVel = R_b.transpose() * u.template segment<3>(0);
    AngularVelocity bodyAngVel = u.template segment<3>(3);

    VectorXd jointVel = u.template segment<12>(6);
    state_unscaled_.template segment<3>(4) = bodyVel.template cast<Dtype>();
    state_unscaled_.template segment<3>(7) = bodyAngVel.template cast<Dtype>();
    state_unscaled_.template segment<12>(10) = jointPosHist_.template segment<12>(48);
    state_unscaled_.template segment<12>(22) = jointPosHist_.template segment<12>(96);
    state_unscaled_.template segment<12>(34) = q.template segment<12>(7).template cast<Dtype>();

    state_unscaled_.template segment<12>(46) = jointVelHist_.template segment<12>(48);
    state_unscaled_.template segment<12>(58) = jointVelHist_.template segment<12>(96);
    state_unscaled_.template segment<12>(70) = u.template segment<12>(6).template cast<Dtype>();
    state_unscaled_.template segment<12>(82) = previousAction_;

    state_unscaled_[94] = headingVel;
    state_unscaled_[95] = lateralVel;
    state_unscaled_[96] = yawrate;

    state = (state_unscaled_ - stateOffset_).cwiseProduct(stateScale_);
  }

  inline RotationMatrix quatToRotMat(Quaternion &q) {
    RotationMatrix R;
    R << q(0) * q(0) + q(1) * q(1) - q(2) * q(2) - q(3) * q(3), 2 * q(1) * q(2) - 2 * q(0) * q(3), 2 * q(0) * q(2)
        + 2 * q(1) * q(3),
        2 * q(0) * q(3) + 2 * q(1) * q(2), q(0) * q(0) - q(1) * q(1) + q(2) * q(2) - q(3) * q(3), 2 * q(2) * q(3)
        - 2 * q(0) * q(1),
        2 * q(1) * q(3) - 2 * q(0) * q(2), 2 * q(0) * q(1) + 2 * q(2) * q(3), q(0) * q(0) - q(1) * q(1) - q(2) * q(2)
        + q(3) * q(3);
    return R;
  }

  State state_;
  State dampedState_;
  Action action_, previousAction_;
  JointSpaceVector torque_, jointNominalConfig;
  LinearVelocity bodyVel_;
  AngularVelocity bodyAngVel_;
  State state_unscaled_; /// just memory slot for computation
  State stateOffset_;
  State stateScale_;
  Action actionOffset_;
  Action actionScale_;
  Action dampedAction_;
  Action scaledAction_;
  Eigen::Matrix<double, 12, 1> dampedJointPos_;
  Eigen::Matrix<double, -1, -1> command_;
  Eigen::Matrix<Dtype, 12*HistoryLength, 1> jointVelHist_, jointPosHist_, tempHist_;
  unsigned long int controlCounter=0, controlRow=0;;
  double headingVelDamped_, lateralVelDamped_, yawrateDamped_;


  RAI::MLP_fullyconnected<stateDimension,actionDimension> mlpNominal_;
  RAI::MLP_fullyconnected<stateDimension,actionDimension> mlpStabilizer_;
};

}

#endif //RAI_QUADRUPEDCONTROLLER_HPP
